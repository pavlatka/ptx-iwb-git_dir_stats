<?php

$data = __DIR__ . '/data';

$files = glob($data . '/*');

$stats = [
  'added'   => 0,
  'deleted' => 0,
  'persons' => []
];

function getFileStats(string $file): array {
  $stats = ['added' => 0, 'deleted' => 0];
  $content = explode("\n", file_get_contents($file));

  foreach ($content as $line) {
    $lineStats = preg_split('/\s+/', trim($line));

    if (!is_numeric($lineStats[0])) {
      continue;
    }

    $stats['added']   += (int)$lineStats[0];
    $stats['deleted'] += (int)$lineStats[1];
  }

  return $stats;
}

function getPersonName(string $file): string {
  $pieces = explode('/', $file);

  return $pieces[count($pieces) - 1];
}

function getPercentage(int $total, int $partial): float {
  return number_format($partial / $total * 100, 2);
}

foreach ($files as $file) {
  $fileStats = getFileStats($file);

  $stats['added']   += $fileStats['added'];
  $stats['deleted'] += $fileStats['deleted'];

  $stats['persons'][getPersonName($file)] = getFileStats($file);
}

echo 'STATS' . PHP_EOL;
echo '-----' . PHP_EOL;
echo '- Added:   ' . $stats['added'] . PHP_EOL;
echo '- Deleted: ' . $stats['deleted'] . PHP_EOL;
echo '- TOTAL:   ' . ($stats['added'] + $stats['deleted']) . PHP_EOL . PHP_EOL;


foreach ($stats['persons'] as $name => $personStats) {
  echo strtoupper($name) . PHP_EOL;
  echo '--------' . PHP_EOL;

  echo '- Added:   ' . $personStats['added'] . ' ' . getPercentage($stats['added'], $personStats['added']) . '%' . PHP_EOL;

  echo '- Deleted: ' . $personStats['deleted'] . ' ' . getPercentage($stats['deleted'], $personStats['deleted']) . '%' . PHP_EOL;

  echo '- TOTAL:   ' . ($personStats['added'] + $personStats['deleted']) . ' ' . getPercentage(($stats['added'] + $stats['deleted']), ($personStats['added'] + $personStats['deleted'])) . '%' . PHP_EOL . PHP_EOL;
}
