#!/bin/bash

FOLDER=/Users/tpavlatka/codebase/Wayfair/sync/php/applications/wms/nexus
DATA=/Users/tpavlatka/codebase/personal/git_dir_stats/data/
WARRIORS=("Tomas Pavlatka" "Maha Omar" Kadar Baesso "Ali Shah")
FILES=(tomas maha ana felipe samad)

for index in "${!WARRIORS[@]}"; do
  cd $FOLDER;
  git log --numstat --oneline --author "${WARRIORS[$index]}" -- $FOLDER >> $DATA/${FILES[$index]};
done
